/* vim: set et ts=8: */

/* gscope-notebook.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-notebook.h"

struct _GScopeNotebook {
        GtkNotebook parent;
};

enum {
        SIG_SYMBOL_SELECTED,
        N_SIG,
};

static guint notebook_signals[N_SIG];

G_DEFINE_TYPE (GScopeNotebook, gscope_notebook, GTK_TYPE_NOTEBOOK)

GScopeNotebook *
gscope_notebook_new (void)
{
        return g_object_new (GSCOPE_TYPE_NOTEBOOK,
                             "visible", TRUE,
                             "margin", 4,
                             "scrollable", TRUE,
                             NULL);
}

static void
sourceview_symbol_selected_cb (GScopeSourceView *source,
                               const gchar      *symbol,
                               guint             num_search_type,
                               gpointer          data)
{
        GScopeNotebook *notebook;

        notebook = GSCOPE_NOTEBOOK (data);

        g_signal_emit (notebook, notebook_signals[SIG_SYMBOL_SELECTED], 0, symbol, num_search_type);
}

static void
close_clicked_cb (GScopeNotebookLabel *label,
                  GtkWidget           *tab,
                  gpointer             data)
{
        GScopeNotebook *notebook;
        gint index_tab;

        notebook = GSCOPE_NOTEBOOK (data);

        index_tab = gtk_notebook_page_num (GTK_NOTEBOOK (notebook), tab);
        gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), index_tab);
}

static gint
get_tab_index (GScopeNotebook *notebook,
               gchar *title)
{
        gint t;

        for (t = 0; t < gtk_notebook_get_n_pages (GTK_NOTEBOOK (notebook)); t++) {
                GtkWidget *tab;
                GtkWidget *tab_label;

                tab = gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), t);
                tab_label = gtk_notebook_get_tab_label (GTK_NOTEBOOK (notebook), tab);

                if (!g_strcmp0 (title, gscope_notebook_label_get_filename (GSCOPE_NOTEBOOK_LABEL (tab_label))))
                        return t;
        }

        return -1;
}

void
gscope_notebook_close_all (GScopeNotebook *notebook)
{
        while ((gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook))) >= 0)
                gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), -1);
}

void
gscope_notebook_open (GScopeNotebook *notebook,
                      GFile          *file,
                      GFile          *path_project,
                      guint           line)
{
        g_autofree gchar *title = NULL;
        GScopeNotebookLabel *tab_label;
        GScopeSourceView *source_view;
        GtkScrolledWindow *scrolled;
        gint tab;

        title = g_file_get_relative_path (path_project, file);

        tab = get_tab_index (notebook, title);
        if (tab < 0) {
                scrolled = GTK_SCROLLED_WINDOW (gtk_scrolled_window_new (NULL, NULL));
                source_view = gscope_source_view_new (file);

                gtk_container_add (GTK_CONTAINER (scrolled), GTK_WIDGET (source_view));
                gtk_widget_show (GTK_WIDGET (scrolled));

                tab_label = gscope_notebook_label_new (GTK_WIDGET (scrolled));
                gscope_notebook_label_set_filename (tab_label, title);

                tab = gtk_notebook_append_page (GTK_NOTEBOOK (notebook), GTK_WIDGET (scrolled),
                                                GTK_WIDGET (tab_label));

                gtk_container_child_set (GTK_CONTAINER (notebook),
                                         GTK_WIDGET (scrolled),
                                         "tab-expand", TRUE,
                                         NULL);

                g_signal_connect (tab_label, "close-clicked", (GCallback) close_clicked_cb, notebook);
                g_signal_connect (source_view, "symbol-selected", (GCallback) sourceview_symbol_selected_cb, notebook);
        } else {
                scrolled = GTK_SCROLLED_WINDOW (gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), tab));
                source_view = GSCOPE_SOURCE_VIEW (gtk_bin_get_child (GTK_BIN (scrolled)));
        }

        gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), tab);

        gscope_source_view_jump_to_line (source_view, line);
}

static void
gscope_notebook_class_init (GScopeNotebookClass *klass)
{
        GtkWidgetClass *widget_class;

        widget_class = GTK_WIDGET_CLASS (klass);

        notebook_signals[SIG_SYMBOL_SELECTED] =
                g_signal_new ("symbol-selected",
                              G_TYPE_FROM_CLASS (widget_class),
                              G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                              0, NULL, NULL, NULL,
                              G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_UINT);
}

static void
gscope_notebook_init (GScopeNotebook *self)
{
}


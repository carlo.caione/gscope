/* vim: set et ts=8: */

/* gscope-window.h
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GSCOPE_TYPE_WINDOW (gscope_window_get_type ())
G_DECLARE_FINAL_TYPE (GScopeWindow, gscope_window, GSCOPE, WINDOW, GtkApplicationWindow)

void    gscope_window_db_created_perc   (GScopeWindow *window,
                                         gdouble       fraction,
                                         GError       *error);

G_END_DECLS

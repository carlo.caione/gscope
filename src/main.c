/* vim: set et ts=8: */

/* main.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gscope-config.h"
#include "gscope-window.h"

static void
check_cscope (GtkWindow *window)
{
        GtkWidget *dialog;
        GtkApplication *app;

        if ((g_find_program_in_path ("cscope")) != NULL)
                return;

        dialog = gtk_message_dialog_new (GTK_WINDOW (window),
                                         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_OK,
                                         "cscope not found in PATH");
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);

        app = gtk_window_get_application (GTK_WINDOW (window));
        g_application_quit (G_APPLICATION (app));
}

static void
on_activate (GtkApplication *app)
{
        GtkWindow *window;

        g_assert (GTK_IS_APPLICATION (app));

        window = gtk_application_get_active_window (app);
        if (window == NULL)
                window = g_object_new (GSCOPE_TYPE_WINDOW,
                                       "application", app,
                                       "default-width", 1024,
                                       "default-height", 768,
                                       NULL);
        gtk_window_present (window);

        check_cscope (window);
}

int
main (int   argc,
      char *argv[])
{
        g_autoptr(GtkApplication) app = NULL;

        bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
        bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        textdomain (GETTEXT_PACKAGE);

        app = gtk_application_new ("org.caione.GScope", G_APPLICATION_FLAGS_NONE);

        g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

        return g_application_run (G_APPLICATION (app), argc, argv);
}

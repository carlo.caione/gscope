/* vim: set et ts=8: */

/* gscope-cscope.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-cscope.h"

struct _GScopeEntry {
        GObject   parent;

        gchar    *file;
        gchar    *func;
        guint     line;
        gchar    *context;
        gchar    *symbol;

        gboolean  is_starred;
};

G_DEFINE_TYPE (GScopeEntry, gscope_entry, G_TYPE_OBJECT)

#define CSCOPE_XREF_FILE  "cscope.out"

const gchar *
gscope_entry_get_file (GScopeEntry *entry)
{
        return entry->file;
}

const gchar *
gscope_entry_get_func (GScopeEntry *entry)
{
        return entry->func;
}

const gchar *
gscope_entry_get_context (GScopeEntry *entry)
{
        return entry->context;
}

const gchar *
gscope_entry_get_symbol (GScopeEntry *entry)
{
        return entry->symbol;
}

guint
gscope_entry_get_line (GScopeEntry *entry)
{
        return entry->line;
}

gboolean
gscope_entry_is_starred (GScopeEntry *entry)
{
        return entry->is_starred;
}

void
gscope_entry_set_starred (GScopeEntry   *entry,
                          gboolean       is_starred)
{
        entry->is_starred = is_starred;
}

static void
gscope_entry_finalize (GObject *obj)
{
        GScopeEntry *entry;

        entry = GSCOPE_ENTRY (obj);

        g_free (entry->file);
        g_free (entry->func);
        g_free (entry->context);
        g_free (entry->symbol);

        G_OBJECT_CLASS (gscope_entry_parent_class)->finalize (obj);
}

static void
gscope_entry_class_init (GScopeEntryClass *klass)
{
        GObjectClass *obj_class = G_OBJECT_CLASS (klass);

        obj_class->finalize = gscope_entry_finalize;
}

static void
gscope_entry_init (GScopeEntry *entry)
{
        entry->is_starred = FALSE;
}

static void
cscope_parse_entry (GScopeEntry *entry,
                    const gchar *str,
                    const gchar *symbol)
{
        gchar **tok;

        tok = g_strsplit_set (str, " ", 4);

        entry->file = g_strdup (tok[0]);
        entry->func = g_strdup (tok[1]);
        entry->line = g_ascii_strtoull (tok[2], NULL, 10);
        entry->context = g_strdup (tok[3]);
        entry->symbol = g_strdup (symbol);

        entry->is_starred = FALSE;

        g_strfreev (tok);
}

GScopeEntry *
gscope_entry_new (const gchar *str,
                  const gchar *symbol)
{
        GScopeEntry *entry;

        entry = g_object_new (GSCOPE_TYPE_ENTRY, NULL);
        cscope_parse_entry (entry, str, symbol);

        return entry;
}

static GSList *
do_search (gchar       *cscope_path,
           const gchar *symbol,
           guint        search_type,
           GError     **error)
{
        g_autoptr(GDataInputStream) data_stream = NULL;
        g_autoptr(GSubprocess) process = NULL;
        g_autofree gchar *cscope_exec = NULL;
        g_autofree gchar *output_line = NULL;
        g_autofree gchar *search = NULL;
        GSList *entry_list = NULL;
        GInputStream *stream;

        cscope_exec = g_find_program_in_path ("cscope");
        
        search = g_strdup_printf("-%d%s", search_type, symbol);
        process = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, error, cscope_exec,
                                    "-dL", "-f", cscope_path, search, NULL);

        if (process == NULL)
                return NULL;

        stream = g_subprocess_get_stdout_pipe (process);
        data_stream = g_data_input_stream_new (stream);

        while ((output_line = g_data_input_stream_read_line (data_stream, NULL, NULL, error)) != NULL) {
                GScopeEntry *entry;

                entry = gscope_entry_new (output_line, symbol);
                entry_list = g_slist_prepend (entry_list, entry);
        }

        return entry_list;
}

GSList *
gscope_cscope_search (GFile        *project_path,
                      const gchar  *symbol,
                      guint         search_type,
                      GError      **error)
{
        g_autofree gchar *proj_path = NULL;
        g_autofree gchar *cscope_path = NULL;

        proj_path = g_file_get_path (project_path);
        cscope_path = g_build_filename (proj_path, CSCOPE_XREF_FILE, NULL);

        return do_search (cscope_path, symbol, search_type, error);
}

gboolean
gscope_cscope_valid_db (GFile *project_path)
{
        g_autoptr(GFile) path_xref = NULL;
        g_autofree gchar *path = NULL;

        path = g_file_get_path (project_path);
        path_xref = g_file_new_build_filename (path, CSCOPE_XREF_FILE, NULL);

        return g_file_query_exists (path_xref, NULL);
}

static void
read_line_cb (GObject      *source_object,
              GAsyncResult *res,
              gpointer      user_data)
{
        g_autofree gchar *output_line = NULL;
        g_autoptr(GError) error = NULL;
        GDataInputStream *data_stream;
        GScopeWindow *window;

        data_stream = G_DATA_INPUT_STREAM (source_object);
        window = GSCOPE_WINDOW (user_data);

        output_line = g_data_input_stream_read_line_finish (data_stream, res, NULL, &error);
        if (output_line == NULL) {
                gscope_window_db_created_perc (window, 1.0, error);
                return;
        }

        if (g_str_has_prefix (output_line, ">")) {
                guint cur;
                guint tot;

                sscanf (output_line, "> Building symbol database %d of %d", &cur, &tot);
                gscope_window_db_created_perc (window, (gdouble) cur / tot, NULL);
        }

        g_data_input_stream_read_line_async (data_stream, G_PRIORITY_DEFAULT,
                                             NULL, read_line_cb, window);
}

void
gscope_cscope_create_db (GScopeWindow *window,
                         GFile        *project_path,
                         GError      **error)
{
        g_autoptr(GDataInputStream) data_stream = NULL;
        g_autoptr(GSubprocess) process = NULL;
        g_autofree gchar *cscope_exec = NULL;
        g_autofree gchar *proj_path = NULL;
        GInputStream *stream;

        cscope_exec = g_find_program_in_path ("cscope");
        proj_path = g_file_get_path (project_path);

        g_chdir (proj_path);
        process = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, error, cscope_exec,
                                    "-Rvlqb", NULL);
        if (process == NULL)
                return;

        stream = g_subprocess_get_stdout_pipe (process);
        data_stream = g_data_input_stream_new (stream);

        g_data_input_stream_read_line_async (data_stream, G_PRIORITY_DEFAULT,
                                             NULL, read_line_cb, window);
}

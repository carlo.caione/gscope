/* vim: set et ts=8: */

/* gscope-sourceview.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-sourceview.h"

#define SYMBOL_CTX "gscope-symbol"

enum {
        ITER_START,
        ITER_END,
        N_ITER,
};

struct _GScopeSourceView {
        GtkSourceView    parent;

        GtkSourceBuffer *source_buffer;
        GtkTextBuffer   *buffer;

        GtkPopover      *popover;

        GtkTextTag      *tag_symbol;
        GtkTextTag      *tag_symbol_selected;
        GtkTextIter      iter_symbol_selected [N_ITER];
        gchar           *symbol;

        GFile           *path_file;
};

enum {
        PROP_PATH_FILE = 1,
        N_PROP,
};

enum {
        SIG_SYMBOL_SELECTED,
        N_SIG,
};

static GParamSpec *source_view_props[N_PROP];
static guint source_view_signals[N_SIG];

G_DEFINE_TYPE (GScopeSourceView, gscope_source_view, GTK_SOURCE_TYPE_VIEW)

void
gscope_source_view_jump_to_line (GScopeSourceView *source,
                                 guint             line)
{
        GtkTextIter iter;
        GtkTextBuffer *text_buffer;

        if (line == 0)
                return;

        text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (source));
        gtk_text_buffer_get_iter_at_line (text_buffer, &iter, line - 1);

        while (gtk_events_pending ())
                gtk_main_iteration ();

        gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (source), &iter, 0, TRUE, 0, 0.50);
        gtk_text_buffer_place_cursor (text_buffer, &iter);
}

static void
enable_cscope_highlight (GScopeSourceView *source)
{
        GtkTextIter start, end;

        gtk_text_buffer_get_start_iter (source->buffer, &start);

        while (gtk_source_buffer_iter_forward_to_context_class_toggle (source->source_buffer,
                                                                       &start,
                                                                       SYMBOL_CTX)) {
                end = start;
                gtk_source_buffer_iter_forward_to_context_class_toggle (source->source_buffer, &end, SYMBOL_CTX);
                gtk_text_buffer_apply_tag (source->buffer, source->tag_symbol, &start, &end);

                start = end;
        }
}

static gboolean
load_file (GScopeSourceView *source)
{
        GtkSourceLanguageManager *lang_manager;
        g_autofree gchar *contents = NULL;
        g_autofree gchar *filename = NULL;
        gboolean enable_highlight;
        GtkSourceLanguage *lang;
        gsize length;

        enable_highlight = FALSE;

        if (!g_file_load_contents (source->path_file, NULL, &contents, &length, NULL, NULL))
                return FALSE;

        filename = g_file_get_path (source->path_file);

        lang_manager = gtk_source_language_manager_get_default ();
        lang = gtk_source_language_manager_guess_language (lang_manager, filename, NULL);

        if ((lang != NULL) &&
            ((!(g_strcmp0 (gtk_source_language_get_name (lang), "C"))) ||
             (!(g_strcmp0 (gtk_source_language_get_name (lang), "C/ObjC Header"))))) {
                gtk_source_buffer_set_language (source->source_buffer, lang);
                gtk_source_buffer_set_highlight_syntax (source->source_buffer, TRUE);
                enable_highlight = TRUE;
        } else {
                gtk_source_buffer_set_language (source->source_buffer, NULL);
                gtk_source_buffer_set_highlight_syntax (source->source_buffer, FALSE);
        }

        gtk_text_buffer_set_text (source->buffer, contents, length);

        while (gtk_events_pending ())
                gtk_main_iteration ();

        return enable_highlight;
}

static void
gscope_source_view_set_file (GScopeSourceView *source,
                             GFile            *path_file)
{
        g_set_object (&source->path_file, path_file);

        if (load_file (source))
                enable_cscope_highlight (source);
}

static void
show_popup (GScopeSourceView *source,
            GtkTextIter      *iter)
{
        GdkRectangle rect;

        gtk_text_view_get_iter_location (GTK_TEXT_VIEW (source), iter, &rect);
        gtk_text_view_buffer_to_window_coords (GTK_TEXT_VIEW (source),
                                               GTK_TEXT_WINDOW_WIDGET,
                                               rect.x, rect.y,
                                               &rect.x, &rect.y);

        gtk_popover_set_pointing_to (source->popover, &rect);
        gtk_popover_popup (source->popover);
}

static void
released_cb (GtkGestureMultiPress *gesture,
             guint                 n_press,
             gdouble               x,
             gdouble               y,
             GScopeSourceView     *source)
{
        GtkTextIter start, end, iter;

        if (gtk_gesture_single_get_button (GTK_GESTURE_SINGLE (gesture)) > 1)
                return;

        gtk_text_buffer_get_selection_bounds (source->buffer, &start, &end);
        if (gtk_text_iter_get_offset (&start) != gtk_text_iter_get_offset (&end))
                return;

        iter = start;

        if (gtk_text_iter_has_tag (&iter, source->tag_symbol)) {
                g_autofree gchar *symbol = NULL;

                if (!gtk_text_iter_toggles_tag (&iter, source->tag_symbol))
                        gtk_text_iter_backward_to_tag_toggle (&start, source->tag_symbol);

                gtk_text_iter_forward_to_tag_toggle (&end, source->tag_symbol);

                symbol = gtk_text_iter_get_text (&start, &end);

                show_popup (source, &iter);

                g_free (source->symbol);
                source->symbol = g_strdup (symbol);

                gtk_text_buffer_remove_tag (source->buffer, source->tag_symbol, &start, &end);
                gtk_text_buffer_apply_tag (source->buffer, source->tag_symbol_selected, &start, &end);

                source->iter_symbol_selected[ITER_START] = start;
                source->iter_symbol_selected[ITER_END] = end;
        }
}

static void
set_cursor_if_appropriate (GScopeSourceView *source,
                           gint              x,
                           gint              y)
{
        static gboolean hovering_symbol = FALSE;
        gboolean hovering = FALSE;
        GtkTextView *text_view;
        GtkTextIter iter;

        text_view = GTK_TEXT_VIEW (source);

        if (gtk_text_view_get_iter_at_location (text_view, &iter, x, y))
                hovering = gtk_text_iter_has_tag (&iter, source->tag_symbol);

        if (hovering != hovering_symbol) {
                GdkDisplay *display;
                GdkWindow *window;
                GdkCursor *cursor;

                hovering_symbol = hovering;

                window = gtk_text_view_get_window (text_view, GTK_TEXT_WINDOW_TEXT);
                display = gdk_window_get_display (window);

                if (hovering_symbol)
                        cursor = gdk_cursor_new_from_name (display, "pointer");
                else
                        cursor = gdk_cursor_new_from_name (display, "default");

                gdk_window_set_cursor (window, cursor);
        }
}

static void
motion_cb (GtkEventControllerMotion *controller,
           gdouble                   x,
           gdouble                   y,
           GScopeSourceView         *source)
{
        gint cx, cy;

        gtk_text_view_window_to_buffer_coords (GTK_TEXT_VIEW (source),
                                               GTK_TEXT_WINDOW_TEXT,
                                               x, y, &cx, &cy);

        set_cursor_if_appropriate (source, cx, cy);
}

static void
popover_closed_cb (GtkPopover *pop,
                   gpointer    data)
{
        GScopeSourceView *source;
        GtkTextIter start, end;

        source = GSCOPE_SOURCE_VIEW (data);

        start = source->iter_symbol_selected[ITER_START];
        end = source->iter_symbol_selected[ITER_END];

        gtk_text_buffer_remove_tag (source->buffer, source->tag_symbol_selected, &start, &end);
        gtk_text_buffer_apply_tag (source->buffer, source->tag_symbol, &start, &end);
}

static void
popover_button_clicked_cb (GtkButton *button,
                           gpointer   data)
{
        GScopeSourceView *source;
        const gchar *search_type;
        guint num_search_type;

        source = GSCOPE_SOURCE_VIEW (data);

        search_type = gtk_widget_get_name (GTK_WIDGET (button));
        num_search_type = g_ascii_strtoull (search_type, NULL, 10);

        gtk_popover_popdown (source->popover);

        g_signal_emit (source, source_view_signals[SIG_SYMBOL_SELECTED], 0, source->symbol, num_search_type);
}

static void
setup_popover (GScopeSourceView *source)
{
        g_autoptr(GtkBuilder) builder = NULL;
        GtkButtonBox *buttonbox;
        GList *button_list;
        GObject *popover;
        GList *button;

        builder = gtk_builder_new ();
        gtk_builder_add_from_resource (builder, "/org/caione/GScope/gscope-popover.ui", NULL);

        popover = gtk_builder_get_object (builder, "popover");
        source->popover = GTK_POPOVER (g_object_ref (popover));
        gtk_popover_set_relative_to (source->popover, GTK_WIDGET (source));

        buttonbox = GTK_BUTTON_BOX (gtk_bin_get_child (GTK_BIN (source->popover)));
        button_list = gtk_container_get_children (GTK_CONTAINER (buttonbox));

        button = button_list;
        while (button) {
                GtkButton *button_sel;

                button_sel = GTK_BUTTON (button->data);
                g_signal_connect (button_sel, "clicked", G_CALLBACK (popover_button_clicked_cb), source);

                button = button->next;
        }

        g_signal_connect (source->popover, "closed", G_CALLBACK (popover_closed_cb), source);
}

static void
setup_symbol_tag (GScopeSourceView *source)
{
        source->tag_symbol =
                gtk_source_buffer_create_source_tag (source->source_buffer, NULL,
                                                     "underline", PANGO_UNDERLINE_SINGLE,
                                                     NULL);

        source->tag_symbol_selected =
                gtk_source_buffer_create_source_tag (source->source_buffer, NULL,
                                                     "weight", PANGO_WEIGHT_BOLD,
                                                     "background", "#F4F6FF",
                                                     "foreground", "#0066BB",
                                                     NULL);
}

static void
setup_scheme (GScopeSourceView *source)
{
        GtkSourceStyleSchemeManager *scheme_manager;
        GtkSourceStyleScheme *scheme;

        scheme_manager = gtk_source_style_scheme_manager_get_default ();
        scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, "solarized-dark");

        gtk_source_buffer_set_style_scheme (source->source_buffer, scheme);
}

static void
setup_lang_manager (void)
{
        GtkSourceLanguageManager *lang_manager;
        g_autofree gchar *clang_dir = NULL;
        const gchar * const *langs;
        int i, lang_files_count;
        gchar **new_langs;

        lang_manager = gtk_source_language_manager_get_default ();
        langs = gtk_source_language_manager_get_search_path (lang_manager);

        lang_files_count = g_strv_length ((gchar **) langs);
        new_langs = g_new (char *, lang_files_count + 2);

        for (i = 0; langs[i]; i++)
                new_langs[i + 1] = g_strdup (langs[i]);

        if (g_file_test (CLANG_DIR, G_FILE_TEST_EXISTS))
                clang_dir = g_strdup(CLANG_DIR);
        else
                clang_dir = g_build_filename (TOP_SRCDIR, "data", "language-specs", NULL);

        new_langs[0] = g_strdup (clang_dir);
        new_langs[lang_files_count + 1] = NULL;

        gtk_source_language_manager_set_search_path (lang_manager, new_langs);

        g_strfreev (new_langs);
}

GScopeSourceView *
gscope_source_view_new (GFile *path_file)
{
        return g_object_new (GSCOPE_TYPE_SOURCE_VIEW,
                             "visible", TRUE,
                             "show-line-numbers", TRUE,
                             "background-pattern", GTK_SOURCE_BACKGROUND_PATTERN_TYPE_GRID,
                             "monospace", TRUE,
                             "cursor-visible", FALSE,
                             "editable", FALSE,
                             "path-file", path_file,
                             "highlight-current-line", TRUE,
                             NULL);
}

static void
gscope_source_view_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
        GScopeSourceView *self;

        self = GSCOPE_SOURCE_VIEW (object);

        switch (prop_id) {
        case PROP_PATH_FILE:
                gscope_source_view_set_file (self, g_value_get_object (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_source_view_get_property (GObject           *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
        GScopeSourceView *self;

        self = GSCOPE_SOURCE_VIEW (object);

        switch (prop_id) {
        case PROP_PATH_FILE:
                g_value_set_object (value, self->path_file);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_source_view_finalize (GObject *object)
{
        GScopeSourceView *view;

        view = GSCOPE_SOURCE_VIEW (object);

        g_clear_object (&view->path_file);
        g_clear_object (&view->popover);

        g_free (view->symbol);

        G_OBJECT_CLASS (gscope_source_view_parent_class)->finalize (object);
}

static void
gscope_source_view_class_init (GScopeSourceViewClass *klass)
{
        GObjectClass *object_class;
        GtkWidgetClass *widget_class;

        object_class = G_OBJECT_CLASS (klass);
        widget_class = GTK_WIDGET_CLASS (klass);

        object_class->set_property = gscope_source_view_set_property;
        object_class->get_property = gscope_source_view_get_property;
        object_class->finalize = gscope_source_view_finalize;

        source_view_props[PROP_PATH_FILE] =
                g_param_spec_object ("path_file",
                                     "File Path",
                                     "File Path",
                                     G_TYPE_FILE,
                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
        g_object_class_install_properties (object_class, N_PROP, source_view_props);

        source_view_signals[SIG_SYMBOL_SELECTED] =
                g_signal_new ("symbol-selected",
                              G_TYPE_FROM_CLASS (widget_class),
                              G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                              0, NULL, NULL, NULL,
                              G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_UINT);

        setup_lang_manager ();
}

static void
gscope_source_view_init (GScopeSourceView *self)
{
        GtkEventController *controller;
        GtkGesture *multi;

        self->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (self));
        self->source_buffer = GTK_SOURCE_BUFFER (self->buffer);

        setup_scheme (self);
        setup_symbol_tag (self);
        setup_popover (self);

        multi = gtk_gesture_multi_press_new (GTK_WIDGET (self));
        controller = GTK_EVENT_CONTROLLER (multi);
        gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (multi), 0);
        g_signal_connect (controller, "released", G_CALLBACK (released_cb), self);

        controller = gtk_event_controller_motion_new (GTK_WIDGET (self));
        g_signal_connect (controller, "motion", G_CALLBACK (motion_cb), self);
}


/* vim: set et ts=8: */

/* gscope-cscope.h
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <gio/gio.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gscope-window.h"

G_BEGIN_DECLS

enum  {
        CSCOPE_C_SYMBOL,
        CSCOPE_FUNC_DEF,
        CSCOPE_FUNC_CALLED_BY,
        CSCOPE_FUNC_CALLING,
        CSCOPE_TEXT,
};

#define GSCOPE_TYPE_ENTRY (gscope_entry_get_type ())
G_DECLARE_FINAL_TYPE (GScopeEntry, gscope_entry, GSCOPE, ENTRY, GObject)

GScopeEntry     *gscope_entry_new               (const gchar   *str,
                                                 const gchar   *symbol);
GSList          *gscope_cscope_search           (GFile         *project_path,
                                                 const gchar   *symbol,
                                                 guint          type,
                                                 GError       **error);
const gchar     *gscope_entry_get_file          (GScopeEntry   *entry);
const gchar     *gscope_entry_get_func          (GScopeEntry   *entry);
const gchar     *gscope_entry_get_context       (GScopeEntry   *entry);
const gchar     *gscope_entry_get_symbol        (GScopeEntry   *entry);
guint            gscope_entry_get_line          (GScopeEntry   *entry);

gboolean         gscope_cscope_valid_db         (GFile         *project_path);
gboolean         gscope_entry_is_starred        (GScopeEntry   *entry);
void             gscope_entry_set_starred       (GScopeEntry   *entry,
                                                 gboolean       is_starred);
void             gscope_cscope_create_db        (GScopeWindow  *window,
                                                 GFile         *project_path,
                                                 GError       **error);

G_END_DECLS

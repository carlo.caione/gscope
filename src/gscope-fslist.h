/* vim: set et ts=8: */

/* gscope-fslist.h
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GSCOPE_TYPE_FS_LIST (gscope_fs_list_get_type ())
G_DECLARE_FINAL_TYPE (GScopeFSList, gscope_fs_list, GSCOPE, FS_LIST, GtkTreeView)

GScopeFSList    *gscope_fs_list_new             (void);
void             gscope_fs_list_set_path        (GScopeFSList *fs_list,
                                                 GFile        *path_project);

G_END_DECLS

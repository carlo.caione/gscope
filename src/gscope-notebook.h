/* vim: set et ts=8: */

/* gscope-notebook.h
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "gscope-sourceview.h"
#include "gscope-notebook-label.h"

G_BEGIN_DECLS

#define GSCOPE_TYPE_NOTEBOOK (gscope_notebook_get_type ())
G_DECLARE_FINAL_TYPE (GScopeNotebook, gscope_notebook, GSCOPE, NOTEBOOK, GtkNotebook)

GScopeNotebook  *gscope_notebook_new            (void);
void             gscope_notebook_open           (GScopeNotebook *notebook,
                                                 GFile          *file,
                                                 GFile          *path_project,
                                                 guint           line);
void             gscope_notebook_close_all      (GScopeNotebook *notebook);

G_END_DECLS

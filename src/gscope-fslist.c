/* vim: set et ts=8: */

/* gscope-fslist.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-fslist.h"

struct _GScopeFSList {
        GtkTreeView  parent;

        GFile       *path_project;
        GFile       *path_current;
};

enum {
        PROP_PATH_PROJECT = 1,
        N_PROP,
};

enum {
        SIG_FILE_SELECTED,
        N_SIG,
};

static GParamSpec *fs_list_props [N_PROP];
static guint fs_list_signals [N_SIG];

G_DEFINE_TYPE (GScopeFSList, gscope_fs_list, GTK_TYPE_TREE_VIEW)

#define PARENT_DIR "Parent directory"

enum {
        COLUMN_ICON,
        COLUMN_FILE,
        COLUMN_SIZE,
        COLUMN_TYPE,
        COLUMN_FG,
        COLUMN_SCALE,
        N_COLUMNS
};

static gint
sort_by_name (gconstpointer a,
              gconstpointer b)
{
        const gchar *str_a;
        const gchar *str_b;

        str_a = g_file_info_get_display_name (G_FILE_INFO (a));
        str_b = g_file_info_get_display_name (G_FILE_INFO (b));

        return g_strcmp0 (str_a, str_b);
}

static void
add_columns (GScopeFSList *fs_list)
{
        GtkCellRenderer *renderer;

        renderer = gtk_cell_renderer_pixbuf_new ();
        gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (fs_list),
                                                     -1,
                                                     "Icon",
                                                     renderer,
                                                     "icon-name", COLUMN_ICON,
                                                     NULL);

        renderer = gtk_cell_renderer_text_new ();
        gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (fs_list),
                                                     -1,
                                                     "File",
                                                     renderer,
                                                     "text", COLUMN_FILE,
                                                     "foreground", COLUMN_FG,
                                                     "scale", COLUMN_SCALE,
                                                     NULL);

        renderer = g_object_new (GTK_TYPE_CELL_RENDERER_TEXT,
                                 "foreground", "gray",
                                 "scale", 0.90,
                                 NULL);
        gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (fs_list),
                                                     -1,
                                                     "Size",
                                                     renderer,
                                                     "text", COLUMN_SIZE,
                                                     NULL);
}

static void
fill_store_with_file_info (gpointer p_info,
                           gpointer p_liststore)
{
        g_autofree gchar *str_bytes = NULL;
        const gchar * const * icon_name;
        const gchar *selected_icon;
        GtkIconTheme *icon_theme;
        GtkListStore *liststore;
        GtkTreeIter iter;
        GFileInfo *info;
        GFileType type;
        GIcon *icon;

        liststore = GTK_LIST_STORE (p_liststore);
        info = G_FILE_INFO (p_info);

        gtk_list_store_insert (liststore, &iter, -1);

        icon = g_file_info_get_icon (info);
        icon_name = g_themed_icon_get_names (G_THEMED_ICON (icon));
        type = g_file_info_get_file_type (info);

        icon_theme = gtk_icon_theme_get_default ();

        selected_icon = icon_name[0];
        if (!gtk_icon_theme_has_icon (icon_theme, icon_name[0]))
                selected_icon = "text-plain";

        if (!(type & G_FILE_TYPE_DIRECTORY))
                str_bytes = g_strdup_printf ("%ld bytes", g_file_info_get_size (info));

        gtk_list_store_set (liststore, &iter,
                            COLUMN_ICON, selected_icon,
                            COLUMN_FILE, g_file_info_get_display_name (info),
                            COLUMN_SIZE, str_bytes,
                            COLUMN_TYPE, type,
                            COLUMN_SCALE, 1.0,
                            -1);
}

static void
add_parent_dir (GScopeFSList *fs_list,
                GtkListStore *liststore)
{
        GtkTreeIter iter;

        if (g_file_equal (fs_list->path_project, fs_list->path_current))
                return;

        gtk_list_store_insert (liststore, &iter, 0);

        gtk_list_store_set (liststore, &iter,
                            COLUMN_ICON, "go-previous",
                            COLUMN_FILE, PARENT_DIR,
                            COLUMN_TYPE, G_FILE_TYPE_DIRECTORY,
                            COLUMN_FG, "gray",
                            COLUMN_SCALE, 0.90,
                            -1);
}

static void
update_model (GScopeFSList *fs_list,
              GFile        *path_current)
{
        g_autoptr(GFileEnumerator) enumerator = NULL;
        g_autoslist(GFileInfo) list_file = NULL;
        g_autoslist(GFileInfo) list_dir = NULL;
        g_autoptr(GError) error = NULL;
        GtkListStore *liststore;
        GFileInfo *info;

        g_set_object (&fs_list->path_current, path_current);

        liststore = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (fs_list)));
        gtk_list_store_clear (liststore);

        enumerator = g_file_enumerate_children (path_current, "*",
                                                G_FILE_QUERY_INFO_NONE,
                                                NULL, &error);
        if (enumerator == NULL)
                goto out;

        while ((info = g_file_enumerator_next_file (enumerator, NULL, &error)) != NULL) {
                GSList **p_list = &list_file;
                GFileType type;

                if (g_file_info_get_is_hidden (info))
                        continue;

                type = g_file_info_get_file_type (info);

                if (type & G_FILE_TYPE_DIRECTORY)
                        p_list = &list_dir;

                *p_list = g_slist_insert_sorted (*p_list, info, sort_by_name);
        }

        if (error != NULL)
                goto out;

        add_parent_dir (fs_list, liststore);
        g_slist_foreach (list_dir, fill_store_with_file_info, liststore);
        g_slist_foreach (list_file, fill_store_with_file_info, liststore);

        gtk_tree_view_columns_autosize (GTK_TREE_VIEW (fs_list));

        return;

out:
        g_print ("Error: %s\n", error->message);
        return;
}

static void
manage_selection (GScopeFSList *fs_list,
                  gchar        *selection,
                  GFileType     type)
{
        g_autoptr(GFile) new_path_current = NULL;

        if (!g_strcmp0 (selection, PARENT_DIR)) {
                new_path_current = g_file_get_parent (fs_list->path_current);
        } else {
                g_autofree gchar *str_path_current = NULL;

                str_path_current = g_file_get_path (fs_list->path_current);
                new_path_current = g_file_new_build_filename(str_path_current,
                                                             selection,
                                                             NULL);
        }

        if (type & G_FILE_TYPE_DIRECTORY)
                update_model (fs_list, new_path_current);
        else
                g_signal_emit (fs_list, fs_list_signals[SIG_FILE_SELECTED],
                               0, new_path_current);
}

static void
tree_row_activated_cb (GtkTreeView       *tree_view,
                       GtkTreePath       *path,
                       GtkTreeViewColumn *column)
{
        GtkTreeSelection *select;
        GtkTreeModel *model;
        GtkTreeIter iter;

        select = gtk_tree_view_get_selection (tree_view);

        if (gtk_tree_selection_get_selected (select, &model, &iter)) {
                g_autofree gchar *selection = NULL;
                GScopeFSList *fs_list;
                GFileType type;

                fs_list = GSCOPE_FS_LIST (tree_view);

                gtk_tree_model_get (model, &iter,
                                    COLUMN_FILE, &selection,
                                    COLUMN_TYPE, &type,
                                    -1);
                manage_selection (fs_list, selection, type);
        }
}

void
gscope_fs_list_set_path (GScopeFSList *fs_list,
                         GFile        *path_project)
{
        g_set_object (&fs_list->path_project, path_project);

        update_model (fs_list, fs_list->path_project);
}

GScopeFSList *
gscope_fs_list_new (void)
{
        return g_object_new (GSCOPE_TYPE_FS_LIST,
                             "activate-on-single-click", TRUE,
                             "enable-search", FALSE,
                             "headers-visible", FALSE,
                             "visible", TRUE,
                             NULL);
}

static void
gscope_fs_list_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
        GScopeFSList *self;

        self = GSCOPE_FS_LIST (object);

        switch (prop_id) {
        case PROP_PATH_PROJECT:
                gscope_fs_list_set_path (self, g_value_get_object (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_fs_list_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
        GScopeFSList *self;

        self = GSCOPE_FS_LIST (object);

        switch (prop_id) {
        case PROP_PATH_PROJECT:
                g_value_set_object (value, self->path_project);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_fs_list_finalize (GObject *object)
{
        GScopeFSList *fs_list;

        fs_list = GSCOPE_FS_LIST (object);

        g_clear_object (&fs_list->path_project);
        g_clear_object (&fs_list->path_current);

        G_OBJECT_CLASS (gscope_fs_list_parent_class)->finalize (object);
}

static void
gscope_fs_list_class_init (GScopeFSListClass *klass)
{
        GObjectClass *object_class;
        GtkWidgetClass *widget_class;
        GtkTreeViewClass *tree_view_class;

        tree_view_class = GTK_TREE_VIEW_CLASS (klass);
        object_class = G_OBJECT_CLASS (klass);
        widget_class = GTK_WIDGET_CLASS (klass);

        object_class->set_property = gscope_fs_list_set_property;
        object_class->get_property = gscope_fs_list_get_property;
        object_class->finalize = gscope_fs_list_finalize;

        tree_view_class->row_activated = tree_row_activated_cb;

        fs_list_props[PROP_PATH_PROJECT] =
                g_param_spec_object ("path_project",
                                     "Project Path",
                                     "Project Path",
                                     G_TYPE_FILE,
                                     G_PARAM_READWRITE);
        g_object_class_install_properties (object_class, N_PROP, fs_list_props);

        fs_list_signals[SIG_FILE_SELECTED] =
                g_signal_new ("file-selected",
                              G_TYPE_FROM_CLASS (widget_class),
                              G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                              0, NULL, NULL, NULL,
                              G_TYPE_NONE, 1, G_TYPE_FILE);
}

static void
gscope_fs_list_init (GScopeFSList *self)
{
        GtkListStore *liststore;

        add_columns (self);

        liststore = gtk_list_store_new (N_COLUMNS,
                                        G_TYPE_STRING,
                                        G_TYPE_STRING,
                                        G_TYPE_STRING,
                                        G_TYPE_UINT,
                                        G_TYPE_STRING,
                                        G_TYPE_FLOAT);
        gtk_tree_view_set_model (GTK_TREE_VIEW (self), GTK_TREE_MODEL (liststore));
}

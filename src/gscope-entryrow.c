/* vim: set et ts=8: */

/* gscope-entryrow.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-entryrow.h"

struct _GScopeEntryRow {
        GtkListBoxRow  parent;

        GtkLabel      *label_context;
        GtkLabel      *label_filename;
        GtkLabel      *label_func;
        GtkLabel      *label_symbol;
        GtkButton     *button_star;
        GtkImage      *image_star;

        GScopeEntry   *entry;
        gboolean       starred;
};

enum {
        PROP_ENTRY = 1,
        PROP_STARRED,
        N_PROP
};

enum {
        SIG_STARRED,
        N_SIG
};

static GParamSpec *row_props[N_PROP];
static guint row_signals[N_SIG];

G_DEFINE_TYPE (GScopeEntryRow, gscope_entry_row, GTK_TYPE_LIST_BOX_ROW)

static void
parse_entry (GScopeEntryRow *row,
             GScopeEntry    *entry)
{
        g_autofree gchar *entry_file_full = NULL;
        g_autofree gchar *entry_func_full = NULL;
        const gchar *entry_context;
        const gchar *entry_file;
        const gchar *entry_func;
        guint entry_line;

        entry_file = gscope_entry_get_file (entry);
        entry_context = gscope_entry_get_context (entry);
        entry_func = gscope_entry_get_func (entry);
        entry_line = gscope_entry_get_line (entry);

        entry_file_full = g_strdup_printf ("%s +%d", entry_file, entry_line);
        entry_func_full = g_strdup_printf ("  %s", entry_func);

        gtk_label_set_text (row->label_filename, entry_file_full);
        gtk_label_set_text (row->label_func, entry_func_full);
        gtk_label_set_text (row->label_context, entry_context);
}

GScopeEntryRow *
gscope_entry_row_new (GScopeEntry *entry,
                      gboolean     starred)
{
        return g_object_new (GSCOPE_TYPE_ENTRY_ROW,
                             "entry", entry,
                             "starred", starred,
                             NULL);
}

GScopeEntry *
gscope_entry_row_get_entry (GScopeEntryRow *row)
{
        return row->entry;
}

gboolean
gscope_entry_row_get_starred (GScopeEntryRow *row)
{
        return row->starred;
}

static void
gscope_entry_row_set_entry (GScopeEntryRow *row,
                            GScopeEntry    *entry)
{
        g_set_object (&row->entry, entry);
        parse_entry (row, entry);
}

static void
button_star_clicked_cb (GtkButton      *button,
                        GScopeEntryRow *row)
{
        g_signal_emit (row, row_signals[SIG_STARRED], 0, row->starred);
}

static void
gscope_entry_row_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
        GScopeEntryRow *self;

        self = GSCOPE_ENTRY_ROW (object);

        switch (prop_id) {
        case PROP_ENTRY:
                gscope_entry_row_set_entry (self, g_value_get_object (value));
                break;
        case PROP_STARRED:
                self->starred = g_value_get_boolean (value);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_entry_row_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
        GScopeEntryRow *self;

        self = GSCOPE_ENTRY_ROW (object);

        switch (prop_id) {
        case PROP_ENTRY:
                g_value_set_object (value, self->entry);
                break;
        case PROP_STARRED:
                g_value_set_boolean (value, self->starred);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_entry_row_finalize (GObject *object)
{
        GScopeEntryRow *row;

        row = GSCOPE_ENTRY_ROW (object);

        g_clear_object (&row->entry);

        G_OBJECT_CLASS (gscope_entry_row_parent_class)->finalize (object);
}

static void
gscope_entry_row_constructed (GObject *object)
{
        GScopeEntryRow *row;

        row = GSCOPE_ENTRY_ROW (object);

        if (!row->starred)
                return;

        gtk_image_set_from_icon_name (row->image_star, "starred-symbolic",
                                      GTK_ICON_SIZE_BUTTON);
        gtk_widget_set_visible (GTK_WIDGET (row->label_symbol), TRUE);
        gtk_label_set_text (row->label_symbol, gscope_entry_get_symbol (row->entry));

        G_OBJECT_CLASS (gscope_entry_row_parent_class)->constructed (object);
}

static void
gscope_entry_row_class_init (GScopeEntryRowClass *klass)
{
        GObjectClass *object_class;
        GtkWidgetClass *widget_class;

        object_class = G_OBJECT_CLASS (klass);
        widget_class = GTK_WIDGET_CLASS (klass);

        object_class->set_property = gscope_entry_row_set_property;
        object_class->get_property = gscope_entry_row_get_property;
        object_class->finalize = gscope_entry_row_finalize;
        object_class->constructed = gscope_entry_row_constructed;

        row_props[PROP_ENTRY] =
                g_param_spec_object ("entry",
                                     "Entry",
                                     "cscope entry",
                                     GSCOPE_TYPE_ENTRY,
                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

        row_props[PROP_STARRED] =
                g_param_spec_boolean ("starred",
                                     "Starred",
                                     "is a starred row",
                                     FALSE,
                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

        g_object_class_install_properties (object_class, N_PROP, row_props);

        row_signals[SIG_STARRED] =
                g_signal_new_class_handler ("starred-change",
                                            G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                                            0, NULL, NULL, NULL,
                                            G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

        gtk_widget_class_set_template_from_resource (widget_class, "/org/caione/GScope/gscope-entryrow.ui");
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, label_func);
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, label_context);
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, label_filename);
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, label_symbol);
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, button_star);
        gtk_widget_class_bind_template_child (widget_class, GScopeEntryRow, image_star);

        gtk_widget_class_bind_template_callback (widget_class, button_star_clicked_cb);
}

static void
gscope_entry_row_init (GScopeEntryRow *self)
{
        gtk_widget_init_template (GTK_WIDGET (self));
}


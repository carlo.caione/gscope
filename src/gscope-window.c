/* vim: set et ts=8: */

/* gscope-window.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-window.h"
#include "gscope-cscope.h"
#include "gscope-entryrow.h"
#include "gscope-fslist.h"
#include "gscope-sourceview.h"
#include "gscope-notebook.h"
#include "gscope-notebook-label.h"

typedef struct {
        guint    load_id;
        guint    current_item;
        guint    items_len;
        GSList  *list;
} GScopeIdleData;

struct _GScopeWindow {
        GtkApplicationWindow     parent;

        GtkHeaderBar            *headerbar;
        GtkButton               *button_open_project;
        GtkButton               *button_stop_loading;
        GtkButton               *button_refresh;
        GtkToggleButton         *button_sidebar_fslist;
        GtkSearchEntry          *search_entry;
        GtkInfoBar              *infobar_cscope_err;
        GtkInfoBar              *infobar_cscope_run;
        GtkLabel                *label_infobar;
        GtkScrolledWindow       *scrolled_fslist;
        GtkScrolledWindow       *scrolled_starred;
        GtkPaned                *paned;
        GtkStackSwitcher        *switcher;
        GtkStack                *stack_side;
        GtkStack                *stack;
        GtkListBox              *listbox;
        GtkListBox              *listbox_starred;
        GtkBox                  *box_switcher;
        GtkBox                  *box_search;
        GtkListBoxRow           *row_empty;
        GtkProgressBar          *progressbar;

        GScopeFSList            *fslist;
        GScopeNotebook          *notebook;
        GScopeEntryRow          *row;
        GScopeIdleData          *idle_data;

        GFile                   *path_project;
};

G_DEFINE_TYPE (GScopeWindow, gscope_window, GTK_TYPE_APPLICATION_WINDOW)

static void
row_starred_change_cb (GScopeEntryRow *row,
                       gboolean        starred,
                       gpointer        data)
{
        GScopeWindow *window;
        gboolean is_starred;
        GScopeEntry *entry;

        window = GSCOPE_WINDOW (data);

        is_starred = gscope_entry_row_get_starred (row);
        entry = gscope_entry_row_get_entry (row);

        if (!is_starred) {
                gboolean already_starred;
                GScopeEntryRow *row_starred;

                already_starred = gscope_entry_is_starred (entry);
                if (already_starred)
                        return;

                row_starred = gscope_entry_row_new (entry, TRUE);

                g_signal_connect (row_starred, "starred-change",
                                  (GCallback) row_starred_change_cb, window);

                gtk_stack_set_visible_child (window->stack_side, GTK_WIDGET (window->scrolled_starred));
                gtk_container_add (GTK_CONTAINER (window->listbox_starred), GTK_WIDGET (row_starred));
                gtk_list_box_unselect_all (window->listbox_starred);
        } else {
                gtk_container_remove (GTK_CONTAINER (window->listbox_starred), GTK_WIDGET (row));
        }

        gscope_entry_set_starred (entry, !is_starred);
}


static void
cleanup_load_items (gpointer data_)
{
        GScopeWindow *window;
        GScopeIdleData *data;

        window = GSCOPE_WINDOW (data_);
        data = window->idle_data;

        g_slist_free_full (data->list, g_object_unref);

        gtk_entry_set_progress_fraction (GTK_ENTRY (window->search_entry), 0);
        gtk_widget_set_sensitive (GTK_WIDGET (window->search_entry), TRUE);
        gtk_widget_hide (GTK_WIDGET (window->button_stop_loading));

        g_free (window->idle_data);
        window->idle_data = NULL;
}

static gboolean
load_items_idle (gpointer data_)
{
        GScopeWindow *window;
        GScopeIdleData *data;
        GScopeEntry *entry;
        GScopeEntryRow *row;

        window = GSCOPE_WINDOW (data_);
        data = window->idle_data;

        gtk_entry_set_progress_fraction (GTK_ENTRY (window->search_entry),
                                         (double) data->current_item / data->items_len);

        if (data->current_item == data->items_len) {
                if (data->items_len == 0)
                        gtk_container_add (GTK_CONTAINER (window->listbox), GTK_WIDGET (window->row_empty));
                return G_SOURCE_REMOVE;
        }

        entry = g_slist_nth_data (data->list, data->current_item);
        row = gscope_entry_row_new (entry, FALSE);
        gtk_container_add (GTK_CONTAINER (window->listbox), GTK_WIDGET (row));

        g_signal_connect (row, "starred-change",
                          (GCallback) row_starred_change_cb, window);

        data->current_item += 1;

        return G_SOURCE_CONTINUE;
}

static void
load_content (GScopeWindow *window,
              GSList       *entry_list)
{
        GScopeIdleData *data;

        data = g_new (GScopeIdleData, 1);
        window->idle_data = data;

        data->list = entry_list;
        data->current_item = 0;
        data->items_len = g_slist_length (entry_list);

        data->load_id = g_idle_add_full (G_PRIORITY_LOW,
                                         load_items_idle,
                                         window,
                                         cleanup_load_items);

        gtk_widget_set_sensitive (GTK_WIDGET (window->search_entry), FALSE);
        gtk_widget_show (GTK_WIDGET (window->button_stop_loading));
}

static void
stop_loading_content (GScopeWindow *window)
{
        if (window->idle_data)
                g_idle_remove_by_data (window);
}

static void
open_file (GScopeWindow *window,
           GFile        *file,
           guint         line)
{
        gscope_notebook_open (window->notebook, file, window->path_project, line);
        gtk_widget_show (GTK_WIDGET (window->notebook));
}

static void
fslist_file_selected_cb (GScopeFSList *fslist,
                         GFile        *file,
                         gpointer      data)
{
        GScopeWindow *window;

        window = GSCOPE_WINDOW (data);

        open_file (window, file, 0);
}

static void
show_error (GScopeWindow *window,
            const gchar *error_str)
{
        gtk_label_set_text (window->label_infobar, error_str);
        gtk_widget_show (GTK_WIDGET (window->infobar_cscope_err));

        gtk_header_bar_set_subtitle (window->headerbar, NULL);
}

static void
search_symbol (GScopeWindow     *window,
               const gchar      *symbol,
               guint             type)
{
        g_autoptr(GError) error = NULL;
        GSList *entry_list = NULL;

        entry_list = gscope_cscope_search (window->path_project, symbol, type, &error);
        if (error != NULL) {
                show_error (window, error->message);
                return;
        }

        load_content (window, entry_list);
}

static void
listbox_remove_entry_row_cb (GtkWidget *widget, gpointer data)
{
        gtk_container_remove (GTK_CONTAINER (data), widget);
}

static void
reset_listbox (GScopeWindow     *window,
               GtkListBox       *listbox)
{
        stop_loading_content (window);
        gtk_container_foreach (GTK_CONTAINER (listbox), listbox_remove_entry_row_cb, listbox);
}

static void
notebook_symbol_selected_cb (GScopeSourceView   *source,
                             const gchar        *symbol,
                             guint               num_pattern,
                             gpointer            data)
{
        GScopeWindow *window;

        window = GSCOPE_WINDOW (data);

        gtk_entry_set_text (GTK_ENTRY (window->search_entry), "");
        reset_listbox (window, window->listbox);

        gtk_entry_set_text (GTK_ENTRY (window->search_entry), symbol);
        gtk_stack_set_visible_child (window->stack_side, GTK_WIDGET (window->box_search));

        search_symbol (window, symbol, num_pattern);
}

static void
setup_code_browsing (GScopeWindow *window,
                     GFile        *path_project)
{
        g_autofree gchar *path = NULL;

        gscope_notebook_close_all (window->notebook);

        path = g_file_get_path (path_project);
        gtk_header_bar_set_subtitle (window->headerbar, path);
        gtk_header_bar_set_title (window->headerbar, "GScope");


        gtk_widget_show (GTK_WIDGET (window->paned));
        gtk_widget_set_sensitive (GTK_WIDGET (window->paned), TRUE);
        gtk_widget_set_sensitive (GTK_WIDGET (window->button_sidebar_fslist), TRUE);
        gtk_widget_set_sensitive (GTK_WIDGET (window->button_refresh), TRUE);
        gtk_toggle_button_set_active (window->button_sidebar_fslist, TRUE);
        gtk_stack_set_visible_child (window->stack_side, GTK_WIDGET (window->scrolled_fslist));
        gtk_stack_set_visible_child (window->stack, GTK_WIDGET (window->paned));

        gtk_entry_set_text (GTK_ENTRY (window->search_entry), "");
        reset_listbox (window, window->listbox);
        reset_listbox (window, window->listbox_starred);

        gscope_fs_list_set_path (window->fslist, path_project);
}

void
gscope_window_db_created_perc (GScopeWindow *window,
                               gdouble       fraction,
                               GError       *error)
{
        if (error != NULL) {
                gtk_widget_hide (GTK_WIDGET (window->infobar_cscope_run));
                show_error (window, error->message);
                return;
        }

        if (fraction >= 1.0) {
                gtk_widget_hide (GTK_WIDGET (window->infobar_cscope_run));
                setup_code_browsing (window, window->path_project);
        } else {
                gtk_progress_bar_set_fraction (window->progressbar, fraction);
        }
}

static void
infobar_run_cscope_clicked_cb (GtkButton    *button,
                               GScopeWindow *window)
{
        g_autoptr(GError) error = NULL;

        gtk_widget_hide (GTK_WIDGET (window->infobar_cscope_err));
        gtk_widget_show (GTK_WIDGET (window->infobar_cscope_run));

        gtk_widget_set_sensitive (GTK_WIDGET (window->paned), FALSE);

        gscope_cscope_create_db (window, window->path_project, &error);
        if (error != NULL) {
                gtk_widget_hide (GTK_WIDGET (window->infobar_cscope_run));
                show_error (window, error->message);
        }
}

static void
button_open_project_cb (GtkButton    *button,
                        GScopeWindow *window)
{
        GtkWidget *dialog;
        gint res;

        dialog = gtk_file_chooser_dialog_new ("Open Project",
                                              GTK_WINDOW (window),
                                              GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                              "Cancel",
                                              GTK_RESPONSE_CANCEL,
                                              "Open",
                                              GTK_RESPONSE_ACCEPT,
                                              NULL);

        res = gtk_dialog_run(GTK_DIALOG (dialog));
        if (res == GTK_RESPONSE_ACCEPT) {
                g_autofree gchar *path_project_uri = NULL;
                g_autoptr(GFile) path_project = NULL;
                GtkFileChooser *chooser;
                gboolean cscope_db;

                chooser = GTK_FILE_CHOOSER (dialog);
                path_project_uri = gtk_file_chooser_get_uri (chooser);
                path_project = g_file_new_for_uri (path_project_uri);

                g_set_object (&window->path_project, path_project);

                cscope_db = gscope_cscope_valid_db (path_project);
                if (!cscope_db)
                        show_error (window, "cscope cross-reference file not found. Do you want to create a new cscope database?");
                else
                        setup_code_browsing (window, path_project);
        }

        gtk_widget_destroy (dialog);
}

static void
search_entry_activate_cb (GtkEntry      *entry,
                          gpointer       user_data)
{
        GScopeWindow *window;
        const gchar *symbol;

        window = GSCOPE_WINDOW (user_data);

        reset_listbox (window, window->listbox);

        symbol = gtk_entry_get_text (entry);
        search_symbol (window, (gchar *) symbol, CSCOPE_C_SYMBOL);
}

static void
search_entry_icon_release_cb (GtkEntry                  *entry,
                              GtkEntryIconPosition       icon_pos,
                              GdkEvent                  *event,
                              gpointer                   user_data)
{
        GScopeWindow *window;

        window = GSCOPE_WINDOW (user_data);

        gtk_entry_set_text (GTK_ENTRY (window->search_entry), "");
        reset_listbox (window, window->listbox);
}

static void
switch_page_cb (GtkNotebook *notebook,
                GtkWidget   *page,
                guint        page_num,
                gpointer     data)
{
        GScopeNotebookLabel *label;
        const gchar *filename;
        GScopeWindow *window;

        window = GSCOPE_WINDOW (data);

        label = GSCOPE_NOTEBOOK_LABEL (gtk_notebook_get_tab_label (notebook, page));
        filename = gscope_notebook_label_get_filename (label);

        gtk_header_bar_set_title (window->headerbar, filename);
}

static void
listbox_row_selected_cb (GtkListBox     *listbox,
                         GtkListBoxRow  *listrow,
                         gpointer        user_data)
{
        g_autofree gchar *path_project_str = NULL;
        g_autoptr(GFile) file = NULL;
        const gchar *path_entry;
        GScopeWindow *window;
        GScopeEntryRow *row;
        GScopeEntry *entry;
        guint line;

        window = GSCOPE_WINDOW (user_data);
        row = GSCOPE_ENTRY_ROW (listrow);

        entry = gscope_entry_row_get_entry (row);
        path_entry = gscope_entry_get_file (entry);
        line = gscope_entry_get_line (entry);

        path_project_str = g_file_get_path (window->path_project);
        file = g_file_new_build_filename (path_project_str, path_entry, NULL);

        open_file (window, file, line);
}

static void
gscope_window_class_init (GScopeWindowClass *klass)
{
        GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

        gtk_widget_class_set_template_from_resource (widget_class, "/org/caione/GScope/gscope-window.ui");
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, headerbar);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, button_open_project);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, button_stop_loading);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, button_sidebar_fslist);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, button_refresh);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, search_entry);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, infobar_cscope_err);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, infobar_cscope_run);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, label_infobar);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, scrolled_fslist);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, scrolled_starred);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, paned);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, switcher);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, listbox);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, listbox_starred);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, box_switcher);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, box_search);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, stack_side);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, stack);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, row_empty);
        gtk_widget_class_bind_template_child (widget_class, GScopeWindow, progressbar);

        gtk_widget_class_bind_template_callback (widget_class, button_open_project_cb);
        gtk_widget_class_bind_template_callback (widget_class, infobar_run_cscope_clicked_cb);
        gtk_widget_class_bind_template_callback (widget_class, search_entry_icon_release_cb);
        gtk_widget_class_bind_template_callback (widget_class, search_entry_activate_cb);
        gtk_widget_class_bind_template_callback (widget_class, listbox_row_selected_cb);
        gtk_widget_class_bind_template_callback (widget_class, stop_loading_content);
}

static void
setup_switcher_cb (GtkWidget *widget, gpointer data)
{
        gtk_widget_set_hexpand (widget, TRUE);
}

static void
gscope_window_init (GScopeWindow *self)
{
        gtk_widget_init_template (GTK_WIDGET (self));

        self->fslist = gscope_fs_list_new ();
        gtk_container_add (GTK_CONTAINER (self->scrolled_fslist), GTK_WIDGET (self->fslist));

        self->notebook = gscope_notebook_new ();
        gtk_paned_add2 (self->paned, GTK_WIDGET (self->notebook));

        g_signal_connect (self->fslist, "file-selected",
                          (GCallback) fslist_file_selected_cb, self);
        g_signal_connect (self->notebook, "switch-page",
                          (GCallback) switch_page_cb, self);
        g_signal_connect (self->notebook, "symbol-selected",
                          (GCallback) notebook_symbol_selected_cb, self);

        g_object_bind_property (self->button_sidebar_fslist, "active",
                                self->box_switcher, "visible",
                                G_BINDING_DEFAULT);

        gtk_container_foreach (GTK_CONTAINER (self->switcher), setup_switcher_cb, NULL);
}

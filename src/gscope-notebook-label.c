/* vim: set et ts=8: */

/* gscope-notebook-label.c
 *
 * Copyright 2018 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gscope-config.h"
#include "gscope-notebook-label.h"

struct _GScopeNotebookLabel {
        GtkBox     parent;

        GtkLabel  *label_filename;
        GtkButton *button_close;

        GtkWidget *tab;
};

enum {
        PROP_WIDGET = 1,
        N_PROP
};

enum {
        SIG_CLOSE,
        N_SIG
};

static GParamSpec *label_props[N_PROP];
static guint label_signals[N_SIG];

G_DEFINE_TYPE (GScopeNotebookLabel, gscope_notebook_label, GTK_TYPE_BOX)

static void
button_close_clicked_cb (GtkWidget           *widget,
                         GScopeNotebookLabel *label)
{
        g_signal_emit (label, label_signals[SIG_CLOSE], 0, label->tab);
}

static void
gscope_notebook_label_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
        GScopeNotebookLabel *self;

        self = GSCOPE_NOTEBOOK_LABEL (object);

        switch (prop_id) {
        case PROP_WIDGET:
                self->tab = g_value_get_object (value);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

static void
gscope_notebook_label_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
        GScopeNotebookLabel *self;

        self = GSCOPE_NOTEBOOK_LABEL (object);

        switch (prop_id) {
        case PROP_WIDGET:
                g_value_set_object (value, self->tab);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
        }
}

void
gscope_notebook_label_set_filename (GScopeNotebookLabel *label,
                                    const gchar         *filename)
{
        gtk_label_set_text (label->label_filename, filename);
}

const gchar *
gscope_notebook_label_get_filename (GScopeNotebookLabel *label)
{
        return gtk_label_get_text (label->label_filename);
}

GScopeNotebookLabel *
gscope_notebook_label_new (GtkWidget *tab)
{
        return g_object_new (GSCOPE_TYPE_NOTEBOOK_LABEL,
                             "tab", tab,
                             NULL);
}

static void
close_clicked_cb (GScopeNotebookLabel *label,
                  GtkWidget *widget)
{
}

static void
gscope_notebook_label_class_init (GScopeNotebookLabelClass *klass)
{
        GObjectClass *object_class;
        GtkWidgetClass *widget_class;

        object_class = G_OBJECT_CLASS (klass);
        widget_class = GTK_WIDGET_CLASS (klass);

        object_class->set_property = gscope_notebook_label_set_property;
        object_class->get_property = gscope_notebook_label_get_property;

        label_props[PROP_WIDGET] =
                g_param_spec_object ("tab",
                                     "Tab",
                                     "The Widget",
                                     GTK_TYPE_WIDGET,
                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);


        g_object_class_install_properties (object_class, N_PROP, label_props);

        label_signals[SIG_CLOSE] =
                g_signal_new_class_handler ("close-clicked",
                                            G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_LAST,
                                            G_CALLBACK (close_clicked_cb),
                                            NULL, NULL, NULL,
                                            G_TYPE_NONE, 1, GTK_TYPE_WIDGET);


        gtk_widget_class_set_template_from_resource (widget_class, "/org/caione/GScope/gscope-notebook-label.ui");
        gtk_widget_class_bind_template_child (widget_class, GScopeNotebookLabel, label_filename);
        gtk_widget_class_bind_template_child (widget_class, GScopeNotebookLabel, button_close);

	gtk_widget_class_bind_template_callback (widget_class, button_close_clicked_cb);
}

static void
gscope_notebook_label_init (GScopeNotebookLabel *self)
{
        gtk_widget_init_template (GTK_WIDGET (self));
}

